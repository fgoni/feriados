<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('trasladables', function () {
    return Feriados::trasladables();
});
Route::get('inamovibles', function () {
    return Feriados::inamovibles();
});
Route::get('nolaborales', function () {
    return Feriados::noLaborales();
});

Route::get('feriados', function () {
    return Feriados::feriados();
});

Route::get('fechas', function () {
    return Feriados::fechas();
});

Route::get('eventos', function () {
    return Feriados::eventos();
});
