@extends('app')
@section('content')
    <div class="overlay">
        <div class="container text-center" height="100%">
            <div class="row">
                <div class="col-md-12">
                    <div id="all" class="disclaimer">

                        <h1>Hoy es <span class="hoyEs">{{ Feriados::hoy()->format('d/m/Y') }}</span></h1>
                        @if (Feriados::esFeriado())
                            <h2 class="feriado">
                                <span class="fa fa-heart"></span> FERIADO <span class="fa fa-heart"></span>
                            </h2>
                        @elseif (Feriados::hoyEsViernes())
                            <h2 class="viernes">
                                VIERNES <span class="fa fa-heart"></span> !
                            </h2>
                        @endif
                        @if (Feriados::cuantoFalta() == 1)
                            <h3>Falta <span class="dias">UN</span> día para el próximo feriado</h3>
                        @else
                            <h3>Faltan <span class="dias">{{ Feriados::cuantoFalta() }}</span> días para el próximo feriado</h3>
                        @endif
                        <h3>
                            {{Feriados::proximoFeriado()->evento}}
                            <br/><br/>
                            {{Feriados::proximoFeriado()->fecha}} ({{ Feriados::proximoFeriado()->diaDeLaSemana }})
                        </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
