<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Feriados <3 !</title>

    <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('/css/feriados.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Fonts -->
    <link href='//fonts.googleapis.com/css?family=Paprika' rel='stylesheet'
          type='text/css'>
    <script data-cfasync="false" src="//fast.eager.io/tFrERrX4gZ.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
@yield('content')

<!-- Scripts -->
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="{{asset('/js/feriados.js')}}"></script>
<script src="{{asset('/js/flowtype.js')}}"></script>
<script>refreshAt(24, 0, 0);</script>
<script>
    var $myGroup = $('#all');
    $myGroup.on('show', '.collapse', function () {
        console.log("Hola");
        $myGroup.find('.collapse').collapse('hide');
    });
</script>
<script>
    $('body').flowtype({
        minimum: 500,
        maximum: 1200,
        minFont: 8,
        maxFont: 20,
        fontRatio: 30
    });
</script>
</body>
</html>
