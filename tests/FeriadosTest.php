<?php

use Carbon\Carbon;

class FeriadosTest extends TestCase
{

    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testSiEsViernes()
    {
        $nextFriday = new Carbon('next friday');
        Feriados::setDay($nextFriday);
        $this->visit('/')->see('VIERNES');
    }

    public function testSiEsFeriado()
    {
        $nextFeriado = Feriados::proximoFeriado();
        Feriados::setDay($nextFeriado->instancia);
        $this->visit('/')->see('FERIADO');
    }

}
